import Vue from "vue";
import Vuex from "vuex";

import states from "@/states";
import Todo from "@/models/Todo";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    todos: [
      new Todo("Optimization of web page", states.NEW),
      new Todo("Install Element-UI", states.NEW),
      new Todo("Add report to dashboard", states.TODO),
      new Todo("Fix bug in home page", states.IN_PROGRESS),
      new Todo("Update framework to the new version", states.DONE),
      new Todo("Create a todo app", states.DONE)
    ],
    STATE: states
  },
  mutations: {
    ADD_TODO(state, todo) {
      if (todo instanceof Todo) state.todos.push(todo);
    },
    DELETE_TODO(state, id) {
      const index = state.todos.findIndex(todo => todo.id === id);
      if (index > -1) state.todos.splice(index, 1);
    },
    UPDATE_TODO(state, todo) {
      const index = state.todos.findIndex(todoItem => todoItem.id === todo.id);
      if (index > -1) Vue.set(state.todos, index, todo);
    }
  },
  actions: {},
  modules: {}
});
