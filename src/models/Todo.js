import uuidv4 from "uuid/v4";
import states from "@/states";

export default class Todo {
  constructor(title, state = states.NEW) {
    this.id = uuidv4();
    this.title = title;
    this.state = state;
  }
}
