import Vue from "vue";
import {
  Container,
  Header,
  Main,
  Row,
  Col,
  Card,
  Drawer,
  Button,
  Form,
  FormItem,
  Input,
  Dropdown,
  DropdownItem,
  DropdownMenu
} from "element-ui";
import App from "./App.vue";
import store from "./store";

Vue.config.productionTip = false;

const components = [
  Container,
  Header,
  Main,
  Row,
  Col,
  Card,
  Drawer,
  Button,
  Form,
  FormItem,
  Input,
  Dropdown,
  DropdownMenu,
  DropdownItem
];
components.forEach(component => component.install(Vue));

new Vue({
  store,
  render: h => h(App)
}).$mount("#app");
