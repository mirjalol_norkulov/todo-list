const states = Object.freeze({
  NEW: "NEW",
  TODO: "TODO",
  DONE: "DONE",
  IN_PROGRESS: "IN_PROGRESS"
});

export default states;
